select
cost_center_number,
cost_center_name,
owner,
manager,
business_lead,
division,
a.fiscal_year fiscal_year,
b.budget_id,
b.budget_name budget_name,
b.budget_code budget_code,
b.budget_desc budget_desc,
b.budget_type budget_type,
b.account_group account_group,
b.account_number account_number,
b.lifecycle lifecycle,
b.vendor_name vendor_name,
b.expense_spread expense_spread,
b.expense_detail expense_detail,
case when budget_type = 'OPEX' then requested_budget  else 0.00 end as opex_requested_budget,
case when budget_type = 'OPEX' then approved_budget  else 0.00 end as opex_approved_budget,
case when budget_type = 'OPEX' then ytd_opex  else 0.00 end as ytd_opex_budget,
case when budget_type = 'CAPEX' then requested_budget  else 0.00 end as capex_requested_budget,
case when budget_type = 'CAPEX' then approved_budget  else 0.00 end as capex_approved_budget,
case when budget_type = 'CAPEX' then ytd_capital  else 0.00 end as ytd_capex_budget
from BKS.enterprise_budget a left outer join BKS.enterprise_budget_detail b on
a.id=b.budget_id
group by cost_center_number, a.fiscal_year
