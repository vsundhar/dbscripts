alter table enterprise_asset modify asset_category varchar(25) null default '';
alter table enterprise_asset modify owner varchar(140) null default '';
alter table enterprise_strategy modify owner varchar(140) null default '';
alter table enterprise_strategy modify strategy_category varchar(140) null default '';
alter table enterprise_strategy modify division varchar(50) null default '';
alter table enterprise_strategy modify manager varchar(140) null default '';
alter table enterprise_strategy modify requestor varchar(140) null default '';
alter table enterprise_strategy modify owner varchar(140) null default '';
alter table enterprise_strategy modify created_timestamp timestamp null default current_timestamp;
alter table enterprise_business_strategy modify owner varchar(140) null default '';
alter table enterprise_business_strategy modify created_timestamp timestamp null default current_timestamp;
alter table enterprise_budget modify created_timestamp timestamp null default current_timestamp;
alter table enterprise_budget modify owner varchar(140) null default '';
alter table enterprise_budget modify division varchar(50) null default '';
alter table enterprise_budget_detail modify created_timestamp timestamp null default current_timestamp;
alter table enterprise_budget_detail modify vendor_name varchar(140) null default '';

