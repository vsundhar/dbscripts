-- MySQL dump 10.13  Distrib 5.6.22, for osx10.8 (x86_64)
--
-- Host: 174.37.242.109    Database: hcris
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a700001`
--

DROP TABLE IF EXISTS `a700001`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a700001` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `DESCRIPTION` varchar(70) NOT NULL,
  `BEG_BAL` varchar(15) DEFAULT NULL,
  `PURCHASES` varchar(15) DEFAULT NULL,
  `DONATION` varchar(15) DEFAULT NULL,
  `TOTAL_I` varchar(15) DEFAULT NULL,
  `DISPOSALS_AND_RETIREMENT` varchar(15) DEFAULT NULL,
  `END_BAL` varchar(15) DEFAULT NULL,
  `FULLY_DEPRECIATED_ASSETS` varchar(15) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_a700001_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_a700001_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`),
  KEY `idx_a700001_PRVDR_NUM` (`PRVDR_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a700002`
--

DROP TABLE IF EXISTS `a700002`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a700002` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(70) NOT NULL,
  `DEPRECIATION` varchar(15) DEFAULT NULL,
  `LEASE` varchar(15) DEFAULT NULL,
  `INTEREST` varchar(15) DEFAULT NULL,
  `INSURANCE` varchar(15) DEFAULT NULL,
  `TAXES` varchar(15) DEFAULT NULL,
  `OTHER_CAPITAL_COST` varchar(15) DEFAULT NULL,
  `TOTAL_II` varchar(15) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_a700002_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_a700002_PRVDR_NUM` (`PRVDR_NUM`),
  KEY `idx_a700002_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a700003`
--

DROP TABLE IF EXISTS `a700003`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a700003` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(70) NOT NULL,
  `GROSS_ASSETS` varchar(15) DEFAULT NULL,
  `CAPITALIZED_LEASES` varchar(15) DEFAULT NULL,
  `GROSS_ASSETS_RATIO` varchar(15) DEFAULT NULL,
  `RATIO` varchar(15) DEFAULT NULL,
  `INSURANCE` varchar(15) DEFAULT NULL,
  `TAXES` varchar(15) DEFAULT NULL,
  `OTHER_CAPITAL_COSTS` varchar(15) DEFAULT NULL,
  `TOTAL` varchar(15) DEFAULT NULL,
  `DEPRECIATION` varchar(15) DEFAULT NULL,
  `LEASE` varchar(15) DEFAULT NULL,
  `INTEREST` varchar(15) DEFAULT NULL,
  `INSURANCE_1` varchar(15) DEFAULT NULL,
  `TAXES_1` varchar(15) DEFAULT NULL,
  `OTHER_CAPITAL_COSTS_1` varchar(15) DEFAULT NULL,
  `TOTAL_1` varchar(15) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_a700003_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_a700003_PRVDR_NUM` (`PRVDR_NUM`),
  KEY `idx_a700003_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `b000000`
--

DROP TABLE IF EXISTS `b000000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b000000` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(70) NOT NULL,
  `CLMN_NUM` char(5) NOT NULL,
  `CLMN_DESC` varchar(70) NOT NULL,
  `CLMN_NMRC` varchar(15) DEFAULT NULL,
  `STATISTICS_DESC` varchar(70) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_b000000_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_b000000_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`),
  KEY `idx_b000000_RPT_REC_NUM_WKSHT_CD` (`RPT_REC_NUM`,`WKSHT_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facility_type`
--

DROP TABLE IF EXISTS `facility_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facility_type` (
  `FROM_CODE` int(11) NOT NULL,
  `TO_CODE` int(11) NOT NULL,
  `FACILITY_TYPE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g000000`
--

DROP TABLE IF EXISTS `g000000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g000000` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(140) NOT NULL,
  `GENERAL_FUND` varchar(15) DEFAULT NULL,
  `SPECIFIC_PURPOSE_FUND` varchar(15) DEFAULT NULL,
  `ENDOWMENT_FUND` varchar(15) DEFAULT NULL,
  `PLANT_FUND` varchar(15) DEFAULT NULL,
  `wksht_cd` char(7) NOT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_g000000_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_g000000_PRVDR_NUM` (`PRVDR_NUM`),
  KEY `idx_g000000_RPT_REC_NUM_LINE_NUM_wksht_cd` (`RPT_REC_NUM`,`LINE_NUM`,`wksht_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g100000`
--

DROP TABLE IF EXISTS `g100000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g100000` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(140) DEFAULT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `GENERAL_FUND_1` varchar(15) DEFAULT NULL,
  `GENERAL_FUND_2` varchar(15) DEFAULT NULL,
  `SPECIFIC_PURPOSE_FUND_1` varchar(15) DEFAULT NULL,
  `SPECIFIC_PURPOSE_FUND_2` varchar(15) DEFAULT NULL,
  `ENDOWMENT_FUND_1` varchar(15) DEFAULT NULL,
  `ENDOWMENT_FUND_2` varchar(15) DEFAULT NULL,
  `PLANT_FUND_1` varchar(15) DEFAULT NULL,
  `PLANT_FUND_2` varchar(15) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_g100000_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_g100000_RPT_REC_NUM_LINE_NUM_WKSHT_CD` (`RPT_REC_NUM`,`LINE_NUM`,`WKSHT_CD`),
  KEY `idx_g100000_PRVDR_NUM` (`PRVDR_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g200000`
--

DROP TABLE IF EXISTS `g200000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g200000` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(140) DEFAULT NULL,
  `INPATIENT` varchar(15) DEFAULT NULL,
  `OUTPATIENT` varchar(15) DEFAULT NULL,
  `TOTAL` varchar(15) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_g200000_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_g200000_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`),
  KEY `idx_g200000_PRVDR_NUM` (`PRVDR_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g300000`
--

DROP TABLE IF EXISTS `g300000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `g300000` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) DEFAULT NULL,
  `LINE_DESC` varchar(140) DEFAULT NULL,
  `REVENUES_AND_EXPENSES` varchar(15) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  KEY `idx_g300000_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_g300000_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`),
  KEY `idx_g300000_PRVDR_NUM` (`PRVDR_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital_rollup`
--

DROP TABLE IF EXISTS `hospital_rollup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital_rollup` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `LABEL` char(30) NOT NULL,
  `ITEM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital_rpt`
--

DROP TABLE IF EXISTS `hospital_rpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital_rpt` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `PRVDR_CTRL_TYPE_CD` char(2) DEFAULT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  `NPI` varchar(11) DEFAULT NULL,
  `RPT_STUS_CD` char(1) NOT NULL,
  `FY_BGN_DT` varchar(10) DEFAULT NULL,
  `FY_END_DT` varchar(10) DEFAULT NULL,
  `PROC_DT` varchar(10) DEFAULT NULL,
  `INITL_RPT_SW` char(1) DEFAULT NULL,
  `LAST_RPT_SW` char(1) DEFAULT NULL,
  `TRNSMTL_NUM` char(2) DEFAULT NULL,
  `FI_NUM` char(5) DEFAULT NULL,
  `ADR_VNDR_CD` char(1) DEFAULT NULL,
  `FI_CREAT_DT` varchar(10) DEFAULT NULL,
  `UTIL_CD` char(1) DEFAULT NULL,
  `NPR_DT` varchar(10) DEFAULT NULL,
  `SPEC_IND` char(1) DEFAULT NULL,
  `FI_RCPT_DT` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital_rpt_alpha`
--

DROP TABLE IF EXISTS `hospital_rpt_alpha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital_rpt_alpha` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `clmn_num` char(5) DEFAULT NULL,
  `ALPHNMRC_ITM_TXT` char(40) NOT NULL,
  KEY `hospital_rpt_alhpa_idx` (`RPT_REC_NUM`,`WKSHT_CD`),
  KEY `hospital_rpt_alpha_idx` (`RPT_REC_NUM`,`WKSHT_CD`,`clmn_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital_rpt_nmrc`
--

DROP TABLE IF EXISTS `hospital_rpt_nmrc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital_rpt_nmrc` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `CLMN_NUM` char(5) NOT NULL,
  `ITM_VAL_NUM` varchar(11) NOT NULL,
  KEY `hospital_rpt_nmrc_idx` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`),
  KEY `hospital_rpt_nmrc_idx_1` (`RPT_REC_NUM`,`WKSHT_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s200001`
--

DROP TABLE IF EXISTS `s200001`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s200001` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  `PRVDR_NAME` varchar(70) NOT NULL,
  `PRVDR_ADDR` varchar(140) NOT NULL,
  `PRVDR_COMP` varchar(255) DEFAULT NULL,
  `PRVDR_GEO_CLASS` char(1) DEFAULT NULL,
  `IPF` char(1) DEFAULT NULL,
  `IRF` char(1) DEFAULT NULL,
  `LTCH` char(1) DEFAULT NULL,
  `CAH` char(1) DEFAULT NULL,
  `MCH` char(1) DEFAULT NULL,
  `CHAIN_PRVDR_NAME` varchar(70) DEFAULT NULL,
  `CHAIN_PRVDR_ADDR` varchar(140) DEFAULT NULL,
  KEY `idx_s200001_RPT_REC_NUM` (`RPT_REC_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s300001`
--

DROP TABLE IF EXISTS `s300001`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s300001` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(70) NOT NULL,
  `NO_OF_BEDS` varchar(15) DEFAULT NULL,
  `BED_DAYS_AVAILABLE` varchar(15) DEFAULT NULL,
  `CAH_HRS` varchar(15) DEFAULT NULL,
  `TOTAL_ALL_PATIENTS` varchar(15) DEFAULT NULL,
  `TOTAL_INTERN_RESIDENTS` varchar(15) DEFAULT NULL,
  `EMPLOYEES_ON_PAYROLL` varchar(15) DEFAULT NULL,
  KEY `idx_s300001_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_s300001_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`),
  KEY `idx_s300001_PRVDR_NUM` (`PRVDR_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s300002`
--

DROP TABLE IF EXISTS `s300002`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s300002` (
  `RPT_REC_NUM` int(11) NOT NULL,
  `WKSHT_CD` char(7) NOT NULL,
  `PRVDR_NUM` char(6) NOT NULL,
  `LINE_NUM` char(5) NOT NULL,
  `LINE_DESC` varchar(70) NOT NULL,
  `ADJUSTED_SALARIES` varchar(15) DEFAULT NULL,
  `PAID_HRS` varchar(15) DEFAULT NULL,
  `AVG_HOURLY_WAGE` varchar(15) DEFAULT NULL,
  KEY `idx_s300002_RPT_REC_NUM` (`RPT_REC_NUM`),
  KEY `idx_s300002_PRVDR_NUM` (`PRVDR_NUM`),
  KEY `idx_s300002_RPT_REC_NUM_WKSHT_CD_LINE_NUM` (`RPT_REC_NUM`,`WKSHT_CD`,`LINE_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `state_codes`
--

DROP TABLE IF EXISTS `state_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_codes` (
  `STATE_NAME` varchar(25) NOT NULL,
  `STATE_CODE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wksht_cd`
--

DROP TABLE IF EXISTS `wksht_cd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wksht_cd` (
  `WKSHT_CD` char(7) DEFAULT NULL,
  `WKSHT_NAME` varchar(25) NOT NULL,
  `WKSHT_DESC` varchar(140) DEFAULT NULL,
  `active` char(1) NOT NULL,
  `PRVDR_NUM` char(6) DEFAULT NULL,
  `RPT_REC_NUM` int(11) NOT NULL,
  `YEAR` char(4) DEFAULT NULL,
  `UPDATED_TIMESTAMP` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MAPPING_CHANGE` char(1) DEFAULT 'N',
  `COMMENT` varchar(255) DEFAULT NULL,
  `FORMAT` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-20 15:07:29
