DELIMITER //
DROP PROCEDURE IF EXISTS createDBSnapshot; //
CREATE PROCEDURE createDBSnapshot(IN DBNAME VARCHAR(6), IN SNAPSHOT_DESC VARCHAR(255), IN CREATED_BY VARCHAR(50), OUT P_SNAPSHOT_NAME varchar(20))
BEGIN
    DECLARE done1 INT DEFAULT FALSE;
    DECLARE tablename TEXT;
    DECLARE stamp varchar(10);
    DECLARE status INT;
    DECLARE cursortable CURSOR FOR (
        SELECT table_name
        FROM information_schema.tables
        WHERE table_schema=DBNAME
        ORDER BY table_name ASC
    );
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

    START TRANSACTION;

    # Get Current Timestamp
    SELECT replace(substring_index(CURRENT_TIMESTAMP,' ',1),'-','') into stamp;
    SET P_SNAPSHOT_NAME := concat(DBNAME,'-',stamp);

    # Drop DB if exists
    IF EXISTS (SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = P_SNAPSHOT_NAME) THEN
      SET @dropinstance := concat("DROP DATABASE `",P_SNAPSHOT_NAME,"`");
      PREPARE dropinstance from @dropinstance;
      EXECUTE dropinstance;
      COMMIT;
      SELECT 'DATABASE DROPPED!!!';
	  END IF;

    # create db
    SET @createinstance := concat("CREATE DATABASE `",P_SNAPSHOT_NAME,"`");
    PREPARE createinstance from @createinstance;
    EXECUTE createinstance;
    COMMIT;

    SELECT 'DATABASE CREATED!!!';

    OPEN cursortable;
    read_loop: LOOP
        FETCH cursortable INTO tablename;
		IF done1 THEN
			LEAVE read_loop;
		END IF;

		SELECT tablename;

		SET @createtable := concat("CREATE TABLE `",P_SNAPSHOT_NAME,"`.`",tablename,"` LIKE `",DBNAME,"`.`",tablename,"`");
		PREPARE createtable from @createtable;
		EXECUTE createtable;

		# Copy Table data
		SET @loadtable := concat("INSERT INTO `",P_SNAPSHOT_NAME,"`.`",tablename,"` SELECT * FROM `",DBNAME,"`.`",tablename,"`");
		PREPARE loadtable from @loadtable;
		EXECUTE loadtable;

		# Update company code
		# SELECT tablename;
		IF(strcmp(tablename, "enterprise_setting_rel") = 0 OR strcmp(tablename, "id") = 0 OR strcmp(tablename, "_debug") = 0) THEN
			SELECT tablename;
		ELSE
			SET @updatetable := concat("UPDATE `",P_SNAPSHOT_NAME,"`.`",tablename,"` SET COMPANY_CODE='",P_SNAPSHOT_NAME,"'");
			SELECT @updatetable;
			PREPARE updatetable from @updatetable;
			EXECUTE updatetable;
		END IF;
    END LOOP;
    CLOSE cursortable;

    # Log into enterprise_snapshot table
    IF(SELECT EXISTS (SELECT 1 FROM enterprise_snapshot WHERE SNAPSHOT_NAME = P_SNAPSHOT_NAME)) THEN
        SET @updatetable := concat("UPDATE `",DBNAME,"`.enterprise_snapshot SET SNAPSHOT_DESC='",SNAPSHOT_DESC,"',CREATED_BY='",CREATED_BY,"',CREATED_TIMESTAMP='",CURRENT_TIMESTAMP,"'");
      # SELECT @updatetable;
      PREPARE updatetable from @updatetable;
      EXECUTE updatetable;
      SELECT 'UPDATED!!!';
      ELSE
      SET @inserttable := concat("INSERT INTO `",DBNAME,"`.enterprise_snapshot(ID, SNAPSHOT_NAME, SNAPSHOT_DESC, COMPANY_CODE, CREATED_BY, CREATED_TIMESTAMP) VALUES(NULL,'",P_SNAPSHOT_NAME,"','",SNAPSHOT_DESC,"','",DBNAME,"','",CREATED_BY,"','",CURRENT_TIMESTAMP,"')");
      # SELECT @inserttable;
      PREPARE inserttable from @inserttable;
      EXECUTE inserttable;
      SELECT 'INSERTED!!!';
    END IF;

    SELECT P_SNAPSHOT_NAME;

    COMMIT;
END //
DELIMITER ;
