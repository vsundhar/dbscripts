source create_db_snapshot.sql
alter table enterprise_rel add column company_code varchar(15) not null;
alter table enterprise_comment modify updated_timestamp timestamp default current_timestamp on update current_timestamp;
alter table enterprise_budget modify column company_code varchar(15) not null;
alter table enterprise_budget_detail modify column company_code varchar(15) not null;
alter table enterprise_invoice_detail modify column company_code varchar(15) not null;
alter table enterprise_element_canonical modify column company_code varchar(15) not null;
