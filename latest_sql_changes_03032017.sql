alter table SE.enterprise modify column parent_company_code varchar(10) null;
alter table SE.enterprise_asset modify id varchar(50) not null;
alter table SE.enterprise_report add column report_parm varchar(255) null;
alter table SE.enterprise_report add column report_container varchar(10) null;
alter table SE.enterprise_filter add column filter_display_name varchar(25) null;
alter table SE.enterprise_filter add column filter_type varchar(10) null;

alter table enterprise_asset modify asset_name varchar(255) not null;
alter table enterprise_strategy modify column enterprise_strategy_desc text CHARACTER SET utf8 COLLATE utf8_general_ci null;
alter table enterprise_strategy modify column enterprise_strategy_activity_name varchar(140) CHARACTER SET utf8 COLLATE utf8_general_ci null;
alter table enterprise_strategy modify column extension text CHARACTER SET utf8 COLLATE utf8_general_ci null;
alter table enterprise_strategy modify column strategy_comment text CHARACTER SET utf8 COLLATE utf8_general_ci null;
alter table enterprise_comment modify column comment text CHARACTER SET utf8 COLLATE utf8_general_ci null;
alter table enterprise_asset change asset_extension extension text CHARACTER SET utf8 COLLATE utf8_general_ci null;
alter table enterprise_business_strategy modify column updated_timestamp timestamp null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table enterprise_business_capability modify column updated_timestamp timestamp null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table enterprise_asset modify updated_timestamp timestamp null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table enterprise_strategy modify column updated_timestamp timestamp null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
alter table enterprise_strategy modify column id varchar(50) not null;
alter table enterprise_business_capability modify id varchar(50) not null;
alter table enterprise_strategy_asset_rel modify enterprise_strategy_id varchar(25) not null;
alter table enterprise_strategy_asset_rel modify asset_id varchar(25) not null;
alter table enterprise_event modify column asset_id varchar(50) not null;
alter table enterprise_comment modify column comp_id varchar(50) not null;
alter table enterprise_asset_business_capability_rel modify column asset_id varchar(50) not null;
alter table enterprise_asset_rel modify column child_asset_id varchar(50) not null;
alter table enterprise_asset_rel modify column parent_asset_id varchar(50) not null;
alter table enterprise_business_strategy modify column id varchar(50) not null; 
alter table enterprise_strategy modify column id varchar(50) not null; 
alter table enterprise_asset modify column id varchar(50) not null;

alter table enterprise_user add column privileges varchar(1024) null;
alter table enterprise_mapping add column mapping_category varchar(25) not null;
alter table enterprise_report add column report_parm varchar(255) null;
alter table enterprise_report add column report_container varchar(10) null;
alter table enterprise_filter add column filter_display_name varchar(25) null;
alter table enterprise_filter add column filter_type varchar(10) null;
alter table enterprise_business_strategy add column strategy_type varchar(25) null DEFAULT 'STRATEGY';
alter table enterprise_strategy add column plan_type varchar(25) null DEFAULT 'PLAN';
alter table enterprise_strategy add column is_activity char(1) null DEFAULT 'N';
alter table enterprise_asset add column asset_type varchar(25) null DEFAULT 'ASSET';


CREATE TABLE SE.enterprise_canonical_names ( id int not null AUTO_INCREMENT, name varchar(255) not null, canonical_name varchar(255) not null, company_code varchar(15) not null DEFAULT 'SE', division varchar(25) null, updated_by varchar(50) not null DEFAULT 'support@smarterd.com', updated_timestamp timestamp NULL DEFAULT NOW() ON UPDATE NOW(), primary key (id) );
insert into enterprise_mapping select * from BKS.enterprise_mapping where id = 22;
update enterprise_mapping set company_code = <company_code>;
insert into enterprise_mapping value(null, "Load Legal Contract Data", "BN_legal_contract_asset_mapping", "BN_Legal_Contract_Asset_Load_Template.csv", "BN", CURRENT_TIMESTAMP, "support@smarterd.com","LOAD LEGAL CONTRACT DATA (ASSET)", "ASSET", "", 2, NULL, "Y");
insert into enterprise_mapping value(null, "Load Business Contract Data", "BN_contract_strategy_mapping", "BN_Contract_Load_Template.csv", "BN", CURRENT_TIMESTAMP, "support@smarterd.com","LOAD CONTRACT DATA (PLAN)", "PLAN", "", 1, NULL, "Y");
update enterprise_mapping set mapping_action='com.smarterd.loader.BNStrategyLoader' where id in(9, 17);
update enterprise_mapping set mapping_action='com.smarterd.loader.BNAssetLoader' where id in(10, 18);
update enterprise_mapping set active = 'N' where id = 14;

update SE.enterprise_filter set filter_name = 'NOT APPROVED', filter_action = 'not_approved' where id = 4;
update SE.enterprise_filter set filter_order = 2 where id = 1;
update SE.enterprise_report set active = 'N' where id = 5;
update SE.enterprise_report set report_name = 'WEEKLY STATUS REPORT (PDF)' where id = 12;

insert into enterprise_filter values(null, 'My Plans', 'List all my approved and active projects', 'PLAN', '', 1, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP,'Y','(owner like "%NAME%" or manager like "%NAME%" or requestor like "%NAME%" or instr(extension, "NAME") > 0)');
insert into enterprise_filter values(null, 'My Agreements', 'List all my 3rd Party Contracts', 'PLAN', '', 1, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP,'Y','strategy_category like "3rd Party Agreement%" and (owner like "%NAME%" or manager like "%NAME%" or requestor like "%NAME%" or instr(extension, "NAME") > 0)');
insert into enterprise_filter values(null, 'My Assets', 'List all my Asssets', 'ASSET', '', 1, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP,'Y','(owner like "%NAME%"');
insert into enterprise_filter values(null, 'Agreements', 'List all Agreements', 'PLAN', '', 2, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP, 'Y', "strategy_category like '3rd Party Agreement%' and (owner like '%' or manager like '%' or requestor like '%' or instr(extension, '%') > 0)");
insert into enterprise_filter values(null, 'Fiscal Year', 'List all Plans or Agreements by Fiscal Year', 'PLAN', '', 2, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP, 'Y', "strategy_category like '%CATEGORY%' and fiscal_year like '%FISCAL_YEAR%'");
insert into enterprise_filter values(null, 'My Agreements', 'List all my 3rd Party Contracts', 'ASSET', '', 1, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP,'Y','a.asset_type like "3rd Party Agreement%" and owner like "%NAME%"');
insert into enterprise_filter values(null, 'Agreements', 'List all Agreements', 'ASSET', '', 2, 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP, 'Y', 'a.asset_type like "3rd Party Agreement%" and owner like "%"');
insert into enterprise_report values(null, 'NEW PROJECTS', 'Display New project details', 'PLAN', 'JASPER', 'BN_new_plan_status', 'BNPlanStatusReportGenerator', 'BN', 'support@smarterd.com', CURRENT_TIMESTAMP, 11, 'PDF', 'Y', 'STAGE=New');
insert into enterprise_report values(null, "IN-FLIGHT PROJECTS", "Display In-Flight project details and status", "PLAN", "JASPER", "BN_inflight_plan_status","generateBNInFlightPlanStatusReport","BN","support@smarterd.com",CURRENT_TIMESTAMP,10,"PDF", "Y", "STAGE=IN-FLIGHT");
update enterprise_report set report_name = 'NEW PROJECTS (PDF)' where id = 1;
update enterprise_report set report_name = 'IN-FLIGHT PROJECTS (PDF)' where id = 2;


 