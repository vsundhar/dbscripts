DELIMITER //
DROP PROCEDURE IF EXISTS updateCompanyCode; //
CREATE PROCEDURE updateCompanyCode(IN DBNAME VARCHAR(10))
BEGIN
    DECLARE done1 INT DEFAULT FALSE;
    DECLARE tablename TEXT;
    DECLARE stamp varchar(10);
    DECLARE status INT;
    DECLARE cursortable CURSOR FOR (
        SELECT table_name FROM information_schema.tables WHERE table_schema=DBNAME ORDER BY table_name ASC
    );
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

    START TRANSACTION;

    OPEN cursortable;
      read_loop: LOOP
        FETCH cursortable INTO tablename;
		  IF done1 THEN
			  LEAVE read_loop;
		  END IF;

		  SELECT tablename;

		IF(strcmp(tablename, "enterprise_setting_rel") = 0 OR strcmp(tablename, "id") = 0 OR strcmp(tablename, "_debug") = 0) THEN
			SELECT tablename;
		ELSE
			SET @updatetable := concat("UPDATE `",DBNAME,"`.`",tablename,"` SET COMPANY_CODE='",DBNAME,"'");
			SELECT @updatetable;
			PREPARE updatetable from @updatetable;
			EXECUTE updatetable;
		END IF;
		  
    END LOOP;
    CLOSE cursortable;

    COMMIT;
END //
DELIMITER ;
