drop table enterprise_budget;
drop table enterprise_budget_detail;
create table enterprise_budget (
id varchar(50) not null,
cost_center_number varchar(15) not null,
cost_center_name varchar(70) null,
owner varchar(70) null,
manager varchar(70) null,
business_lead varchar(70) null,
division varchar(25) null,
lifecycle varchar(25) null,
fiscal_year varchar(4) not null,
estimated_capex varchar(25) null,
ytd_capital varchar(25) null,
estimated_opex varchar(25) null,
ytd_opex varchar(25) null,
extension text null,
created_timestamp timestamp null default current_timestamp,
created_by varchar(50) null,
updated_timestamp timestamp null default current_timestamp,
updated_by varchar(50) null,
company_code varchar(10) not null,
primary key(cost_center_number, fiscal_year)
);

create table enterprise_budget_detail (
id varchar(50) not null,
budget_id varchar(50) not null,
budget_name varchar(1024) null,
budget_code varchar(70) null,
budget_desc varchar(1024) null,
budget_type varchar(10) not null,
account_group varchar(70) null,
account_number varchar(15) null,
vendor_name varchar(140) null,
comment text null,
fiscal_year varchar(4) null,
requested_budget varchar(25) null,
approved_budget varchar(25) null,
committed_budget varchar(25) null,
ytd_spend varchar(25) null,
expense_spread varchar(15) null,
lifecycle varchar(25) null,
expense_detail text null,
extension text null,
created_timestamp timestamp null default current_timestamp,
created_by varchar(50) null,
updated_timestamp timestamp null default current_timestamp,
updated_by varchar(50) null,
company_code varchar(10) not null,
primary key(id)
);
insert into enterprise_budget(id,cost_center_number,cost_center_name,fiscal_year,division,owner,manager,business_lead,lifecycle,created_by,company_code)
select uuid(),input_code,output_name,'FY17',trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CORP CODE"'), '$', ''), ',',''),'\"','')),
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Kristine Eckels' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CC OWNER\"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Kacey Sharrett' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"VP"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Fred Argir' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"BUSINESS LEAD\"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'FINANCE USE ONLY' when output_name = 'REVIEW' then 'REVIEW' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) end,
'support@smarterd.com','BKS' from enterprise_element_canonical where input_type = 'Cost Center' and output_type = 'Business Area' and trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) != 'Inactive';
insert into enterprise_budget(id,cost_center_number,cost_center_name,fiscal_year,division,owner,manager,business_lead,lifecycle,created_by,company_code)
select uuid(),input_code,output_name,'FY18',trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CORP CODE"'), '$', ''), ',',''),'\"','')),
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Kristine Eckels' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CC OWNER\"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Kacey Sharrett' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"VP"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Fred Argir' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"BUSINESS LEAD\"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'FINANCE USE ONLY' when output_name = 'REVIEW' then 'REVIEW' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) end,
'support@smarterd.com','BKS' from enterprise_element_canonical where input_type = 'Cost Center' and output_type = 'Business Area' and trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) != 'Inactive';
insert into enterprise_budget(id,cost_center_number,cost_center_name,fiscal_year,division,owner,manager,business_lead,lifecycle,created_by,company_code)
select uuid(),input_code,output_name,'FY19',trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CORP CODE"'), '$', ''), ',',''),'\"','')),
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Kristine Eckels' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CC OWNER\"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Kacey Sharrett' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"VP"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'Mark Hirsch' when output_name = 'REVIEW' then 'Fred Argir' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"BUSINESS LEAD\"'), '$', ''), ',',''),'\"','')) end,
case when output_name = 'Finance' then 'FINANCE USE ONLY' when output_name = 'REVIEW' then 'REVIEW' else trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) end,
'support@smarterd.com','BKS' from enterprise_element_canonical where input_type = 'Cost Center' and output_type = 'Business Area' and trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) != 'Inactive';

/* Sample Template for all initialization */
insert into enterprise_budget(id,cost_center_number,cost_center_name,fiscal_year,division,owner,manager,business_lead,lifecycle,created_by,company_code)
select uuid(),input_code,output_name,'FY19',trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CORP CODE"'), '$', ''), ',',''),'\"','')), trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"CC OWNER\"'), '$', ''), ',',''),'\"','')),
trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"VP"'), '$', ''), ',',''),'\"','')),
trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"BUSINESS LEAD\"'), '$', ''), ',',''),'\"','')),
trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')),
'support@smarterd.com','DEMO' from enterprise_element_canonical where input_type = 'Cost Center' and output_type = 'Business Area' and trim(replace(replace(replace(JSON_EXTRACT(extension, '$.\"LIFECYCLE STAGE\"'), '$', ''), ',',''),'\"','')) != 'Inactive';
