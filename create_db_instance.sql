DELIMITER //
DROP PROCEDURE IF EXISTS createDBInstance; //
CREATE PROCEDURE createDBInstance(IN DBNAME VARCHAR(10), IN DOMAIN_NAME VARCHAR(25))
BEGIN
    DECLARE done1 INT DEFAULT FALSE;
    DECLARE tablename TEXT;
    DECLARE stamp varchar(10);
    DECLARE status INT;
    DECLARE cursortable CURSOR FOR (
        SELECT table_name FROM information_schema.tables WHERE table_schema='DEMO' ORDER BY table_name ASC
    );
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

    SET DBNAME := UPPER(DBNAME);

    START TRANSACTION;

    # Drop DB if exists
    IF EXISTS (SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = DBNAME) THEN
		  SET @dropinstance := concat("DROP DATABASE `",DBNAME,"`");
		  PREPARE dropinstance from @dropinstance;
		  EXECUTE dropinstance;
		  COMMIT;
		  SELECT 'DATABASE DROPPED!!!';
	  END IF;

    # create db
    SET @createinstance := concat("CREATE DATABASE `",DBNAME,"`");
    PREPARE createinstance from @createinstance;
    EXECUTE createinstance;
    COMMIT;

    SELECT 'DATABASE CREATED!!!';

    OPEN cursortable;
      read_loop: LOOP
        FETCH cursortable INTO tablename;
		  IF done1 THEN
			  LEAVE read_loop;
		  END IF;

		  SELECT tablename;

		  SET @createtable := concat("CREATE TABLE `",DBNAME,"`.",tablename," LIKE "," DEMO.",tablename);

		  PREPARE createtable from @createtable;
		  EXECUTE createtable;
    END LOOP;
    CLOSE cursortable;

    SET @inserttable := concat("INSERT INTO `",DBNAME,"`.enterprise_business_capability select * from DEMO.enterprise_business_capability");
		PREPARE inserttable from @inserttable;
		EXECUTE inserttable;

		SET @updatetable := concat("UPDATE `",DBNAME,"`.enterprise_business_capability set company_code='",DBNAME,"'");
		PREPARE updatetable from @updatetable;
		EXECUTE updatetable;

		SET @inserttable := concat("INSERT INTO `",DBNAME,"`.enterprise_setting select * from DEMO.enterprise_setting");
		PREPARE inserttable from @inserttable;
		EXECUTE inserttable;

		SET @updatetable := concat("UPDATE `",DBNAME,"`.enterprise_setting set company_code='",DBNAME,"'");
		PREPARE updatetable from @updatetable;
		EXECUTE updatetable;

		SET @inserttable := concat("INSERT INTO `",DBNAME,"`.enterprise_mapping select * from DEMO.enterprise_mapping");
		PREPARE inserttable from @inserttable;
		EXECUTE inserttable;

		SET @updatetable := concat("UPDATE `",DBNAME,"`.enterprise_mapping set company_code='",DBNAME,"'");
		PREPARE updatetable from @updatetable;
		EXECUTE updatetable;

		SET @inserttable := concat("INSERT INTO `",DBNAME,"`.enterprise_filter select * from DEMO.enterprise_filter");
		PREPARE inserttable from @inserttable;
		EXECUTE inserttable;

		SET @updatetable := concat("UPDATE `",DBNAME,"`.enterprise_filter set company_code='",DBNAME,"'");
		PREPARE updatetable from @updatetable;
		EXECUTE updatetable;

		SET @inserttable := concat("INSERT INTO `",DBNAME,"`.enterprise_report select * from DEMO.enterprise_report");
		PREPARE inserttable from @inserttable;
		EXECUTE inserttable;

		SET @updatetable := concat("UPDATE `",DBNAME,"`.enterprise_report set company_code='",DBNAME,"'");
		PREPARE updatetable from @updatetable;
		EXECUTE updatetable;

    COMMIT;
END //
DELIMITER ;
