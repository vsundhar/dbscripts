delete from SE.enterprise_canonical_names;
LOAD DATA LOCAL INFILE '/Users/vsundhar/dumps/canonical_vendor_master.csv ' INTO TABLE SE.enterprise_canonical_names
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(canonical_name, name);

delete from enterprise_element_canonical;
LOAD DATA LOCAL INFILE '/Users/vsundhar/Downloads/enterprise_element_canonical-2017.07.28.csv' INTO TABLE enterprise_element_canonical
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\r'
IGNORE 1 LINES
(input_code, input_name, input_type, output_name, output_type, company_code, updated_by);

select internal_id, asset_name, asset_category, owner, JSON_EXTRACT(extension, '$."Plan 1"'), JSON_EXTRACT(extension, '$."Expiration Type"') as Active, asset_startdate from enterprise_asset where asset_category = 'CONTENT' and (JSON_EXTRACT(extension, '$."Expiration Type"') = 'Autorenewal' or JSON_EXTRACT(extension, '$."Expiration Type"') = 'Evergreen/No-expiration') and asset_type = 'AGREEMENT' and asset_name like '%ebook%';