delete from enterprise_user where email != 'duser@smarterd.com';
update enterprise_user set company_code = 'DEMO';
delete from enterprise_report;
delete from enterprise_comment;
delete from enterprise_mapping;
delete from enterprise_notification;
delete from enterprise_snapshot;
delete from enterprise_log;
delete from enterprise_event;
delete from enterprise_service;
delete from enterprise_filter;
delete from enterprise_business_capability;
delete from enterprise_business_strategy;
delete from enterprise_strategy;
delete from enterprise_strategy_asset_rel;
delete from enterprise_asset;
delete from enterprise_asset_rel;
delete from enterprise_asset_business_capability_rel;
delete from enterprise_rel;
delete from enterprise_budget;
delete from enterprise_budget_detail;
delete from enterprise_invoice_detail;
delete from enterprise_resource;
delete from enterprise_usage;
delete from enterprise_governance;
delete from enterprise_setting where setting_category = 'VENDOR' and setting_name is not null;
commit;
